package com.iut1.easy_rando.model

import com.google.firebase.database.PropertyName
import com.google.gson.annotations.SerializedName

data class Coordinate (
    @PropertyName("origin") @SerializedName("origin") var origin: Array<Point>,
    @PropertyName("destination") @SerializedName("destination") var destination: Array<Point>
    ) {
    constructor() : this(arrayOf(Point()), arrayOf(Point()))
}