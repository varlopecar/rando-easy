package com.iut1.easy_rando.model

import com.google.firebase.database.PropertyName
import com.google.gson.annotations.SerializedName

class Point(
    @PropertyName("latitude") @SerializedName("latitude") var latitude: Double,
    @PropertyName("longitude") @SerializedName("longitude") var longitude: Double
) {
    constructor() : this(0.0, 0.0)
}