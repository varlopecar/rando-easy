package com.iut1.easy_rando.model

import com.google.firebase.database.PropertyName
import com.google.gson.annotations.SerializedName

data class Route (
    @PropertyName("title") @SerializedName("title") var title: String,
    @PropertyName("localization") @SerializedName("localization") var localization: String,
    @PropertyName("image") @SerializedName("image") var image: String,
    @PropertyName("difficultLevel") @SerializedName("difficultLevel") var difficultLevel: String,
    @PropertyName("description") @SerializedName("description") var description: String,
    @PropertyName("coordinates") @SerializedName("coordinates") var coordinates: HashMap<String, Any?>
    ) {
    constructor() : this("", "", "", "", "", HashMap<String, Any?>())
}