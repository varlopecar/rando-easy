package com.iut1.easy_rando

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.iut1.easy_rando.model.CourseModel
import com.iut1.easy_rando.ui.CourseAdapter
import com.iut1.easy_rando.ui.DescriptionRoute

class MainActivity : AppCompatActivity(), CourseAdapter.CardClickInterface {


    override fun onCreate(savedInstanceState: Bundle?) {
        val listtitre = arrayListOf("");

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // get firestore instance
        val db = FirebaseFirestore.getInstance()

        val courseRV = findViewById<RecyclerView>(R.id.idRVCourse)

        var u=0
        // Here, we have created new array list and added data to it
        val courseModelArrayList: ArrayList<CourseModel> = ArrayList<CourseModel>()
        courseModelArrayList.add(CourseModel("DSA in Java", 4, R.drawable.gfgimage))
        courseModelArrayList.add(CourseModel("Bastille", 4, R.drawable.gfgimage))
        courseModelArrayList.add(CourseModel("zen", 4, R.drawable.gfgimage))
        // we are initializing our adapter class and passing our arraylist to it.
        val courseAdapter = CourseAdapter(this,this, courseModelArrayList)


        // below line is for setting a layout manager for our recycler view.
        // here we are creating vertical list so we will provide orientation as vertical
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        // in below two lines we are setting layoutmanager and adapter to our recycler view.
        courseRV.layoutManager = linearLayoutManager
        courseRV.adapter = courseAdapter

        // search implement
        var i =0
        for (name in courseModelArrayList) {
            listtitre.add(courseModelArrayList.get(i).getCourse_name())
            i++
        }


        // search bar
        val autoTextView: AutoCompleteTextView = findViewById(R.id.autoCompleteTextView)
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.select_dialog_item, listtitre)
        autoTextView.threshold = 0
        autoTextView.setAdapter(adapter)

        // empty
        autoTextView.addTextChangedListener {

            if ( u != 0) {
                courseModelArrayList.clear()
                courseModelArrayList.add(CourseModel("DSA in Java", 4, R.drawable.gfgimage))
                courseModelArrayList.add(CourseModel("Bastille", 4, R.drawable.gfgimage))
                courseModelArrayList.add(CourseModel("zen", 4, R.drawable.gfgimage))

                // in below two lines we are setting layoutmanager and adapter to our recycler view.
                courseRV.layoutManager = linearLayoutManager
                courseRV.adapter = courseAdapter

            }

        }

        //onclick
        autoTextView.setOnItemClickListener(OnItemClickListener { parent, view, position, id ->
            Toast.makeText(
                applicationContext,
                "Votre recherche '" + adapter.getItem(position)+"' a bien été pris en compte!",
                Toast.LENGTH_SHORT
            ).show()

            var var8 = adapter.getItem(position)
            u++

            courseModelArrayList.clear()
            courseModelArrayList.add(CourseModel(var8.toString(), 4, R.drawable.gfgimage))


            // in below two lines we are setting layoutmanager and adapter to our recycler view.
            courseRV.layoutManager = linearLayoutManager
            courseRV.adapter = courseAdapter

        })


    }

    fun goDescriptionRandonnee(var1: String){
        val intent = Intent(this@MainActivity, DescriptionRoute::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        intent.putExtra("info" , var1)
        startActivity(intent)
    }

    override fun onCardCLick(courseModel: CourseModel) {
        val objetenstring =  Gson().toJson(courseModel)  // json string

        goDescriptionRandonnee(objetenstring)

    }
}
