package com.iut1.easy_rando.ui

import android.os.Bundle
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.iut1.easy_rando.R

class Card: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.card_layout)

        val rl = findViewById<RelativeLayout>(R.id.Card)

        rl.setOnClickListener {
            setContentView(R.layout.activity_description_route)
        }

    }
}