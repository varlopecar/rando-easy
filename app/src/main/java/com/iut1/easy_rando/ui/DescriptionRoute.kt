package com.iut1.easy_rando.ui

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.iut1.easy_rando.R
import com.iut1.easy_rando.model.CourseModel
import com.iut1.easy_rando.model.Route

class DescriptionRoute: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description_route)

        val db = Firebase.firestore

        db.collection("Routes").get().addOnSuccessListener { result ->
            for (document in result) {
                val route = document.toObject(Route::class.java)
                Log.wtf("wtf", route.toString())
            }
        }.addOnFailureListener { exception ->
            Log.w("firebase", "Error getting documents.", exception)
        }

        val titre = findViewById<TextView>(R.id.titre_rando)
        val dificulte = findViewById<TextView>(R.id.dificulte)
        val temps = findViewById<TextView>(R.id.distance)
        val description = findViewById<TextView>(R.id.description)


        val studcourseent = Gson().fromJson(intent.extras?.getString("info"), CourseModel::class.java)

        titre.setText( studcourseent.getCourse_name())


    }
}