package com.iut1.easy_rando.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.iut1.easy_rando.R
import com.iut1.easy_rando.model.CourseModel

class CourseAdapter (private val context: Context, val oncardClickInterface: CardClickInterface, courseModelArrayList: ArrayList<CourseModel>) :
    RecyclerView.Adapter<CourseAdapter.Viewholder>() {

        private val courseModelArrayList: ArrayList<CourseModel>
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
            // to inflate the layout for each item of recycler view.
            val view: View = LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
            return Viewholder(view)
        }

        override fun onBindViewHolder(holder: Viewholder, position: Int) {
            // to set data to textview and imageview of each card layout
            val model: CourseModel = courseModelArrayList[position]
            holder.courseNameTV.setText(model.getCourse_name())
            holder.courseRatingTV.setText("" + model.getCourse_rating())
            holder.courseIV.setImageResource(model.getCourse_image())
            holder.cardCourse.setOnClickListener {
                //val mainActivity = context as MainActivity
                // mainActivity.goDescriptionRandonnee()
                oncardClickInterface.onCardCLick(courseModelArrayList[position])
            }
        }

        override fun getItemCount(): Int {
            // this method is used for showing number of card items in recycler view.
            return courseModelArrayList.size
        }

        // View holder class for initializing of your views such as TextView and Imageview.
        class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val courseIV: ImageView
            val courseNameTV: TextView
            val courseRatingTV: TextView
            val cardCourse: RelativeLayout
            init {
                courseIV = itemView.findViewById(R.id.idIVCourseImage)
                courseNameTV = itemView.findViewById(R.id.idTVCourseName)
                courseRatingTV = itemView.findViewById(R.id.idTVCourseRating)
                cardCourse = itemView.findViewById(R.id.Card)
            }
        }
        // Constructor
        init {
            this.courseModelArrayList = courseModelArrayList
        }

        interface CardClickInterface {
            fun onCardCLick(courseModel: CourseModel)
        }
}
